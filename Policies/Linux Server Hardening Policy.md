[![AVANTPAGE](https://i.imgur.com/so7xu2H.png)](AVANTPAGE) 


## **Linux Server Hardening Policy**

#### **About this document**
This document describes policy for hardening Security on Linux Servers for Avantpage Inc.
The process provides a consistent methods for administrators to follow when hardening Linux Servers for Avantpage Inc.

#### **Who should use this document?**
This document should be used by:
 - Avantpage Inc personnel responsible for administration of servers
 - Avantpage Inc personnel involved in the operation and management of servers

### **Security Options**
***

| Name                                | Option  |
| ----------------------------------- | ------- |
| Secure Shared Memory                | Yes     |
| Disc Encryption                     | Yes     |
| Firewall                            | Yes     |
| Strong Passwords Only               | Yes     |
| SSH Root Login                      | No      |
| SSH User Login                      | Yes     |
| SSH Password Login                  | No      |
| SSH Key-pair Passpharse             | Yes     |
| SSH Emergency Access Policy         | Yes     |
| Root Account Locked                 | Yes     |
| New User with Sudo Privileges       | Yes     |
| Protect su                          | Yes     |
| Prevent IP Spoofing                 | Yes     |
| ICMP Broadcast Requests             | Disable |
| ICMP Redirects                      | Disable |
| Source Packet Routing               | Disable |
| Send Redirects                      | Ignore  |
| SYN Attacks                         | Block   |
| Log Martians                        | Yes     |
| Directed Pings                      | Ignore  |
| Open DNS Recursion                  | Disable |
| Open DNS Remove Version Info        | Enable  |
| PHP Policy                          | Yes     |
| SSL/TLS Policy                      | Yes     |
| Restrict Apache Information Leakage | Enable  |
| Port Management Policy              | Yes     |
| Software Version Policy             | Yes     |
| Protocol Management Policy          | Yes     |



***