[![AVANTPAGE](https://i.imgur.com/so7xu2H.png)](AVANTPAGE) 


## **Linux Server Management Attachment**


#### **About this document**
This document describes a detailed information of Linux Servers in Avantpage Inc.

#### **Who should use this document?**
This document should be used by:
 - Avantpage Inc personnel responsible for administration of servers
 - Avantpage Inc personnel involved in the operation and management of servers

### **ESET Remote Administrator**
***

#### **About Server**
ESET Remote Administrator is an antivirus server which purpose is to manage hosts with installed ESET Endpoint Security.

| Hostname     | eldir                       |
|------------- | --------------------------- |
| URL          | https://eldir.avantpage.com |
| IPv4 Address | 184.171.161.245             |
| IPv6 Address | N/A                         |
| DNS Server 1 | 8.8.8.8                     |
| DNS Server 2 | 8.8.4.4                     |
| Subnet Mask  | 255.255.255.255             |
| Gateway      | 184.171.161.246             |
| MAC Address  | 01:23:45:67:89:ab           |

| Server Type   | Bare metal     |
| --------------| -------------- |
| Location      | Phoenix        |
| IP Allocation | /29 VLAN 5 IPs |
| CPU           | Xeon           |
| RAM           | 32 GB          |
| Motherboard   |                |
| SSD           | 250 GB         |
| HDD           | 2 TB           |
| RAID          | 0              |

| Operating System | CentOS 7 |
|----------------- | -------- |
| Control Panel    | Webmin   |
| Database         | MySQL    |

#### **System Services**
- Ansible
- docker
- OpenSSH
- MySQL

***