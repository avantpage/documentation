[![AVANTPAGE](https://i.imgur.com/so7xu2H.png)](AVANTPAGE) 

## **Incident Management Process**
***

#### **About this document**
This document describes the Incident Management Process.
The process provides a consistent method for everyone to follow when issues regarding services from Avantpage Inc are reported.

#### **Who should use this document?**
This document should be used by:
 - Avantpage Inc personnel responsible for the restoration of services
 - Avantpage Inc personnel involved in the operation and management of Incident Process

### **Chapter 1 - Incident Process**
***

#### **Primary Goal**
The primary goal of the Incident Management process is to restore normal service operation as quickly as possible and minimize the adverse impact on business operations, thus ensuring that the best possible levels of service quality and availability are maintained. _Normal service operation_ is defined here as functional service.

#### **Process Definition**
Incident Management includes any event which disrupts, or which could disrupt, a service. This includes events which are communicated directly by users or Avantpage Inc personnel through the Service Desk or through an interface from Event Management to Incident Management tools.

#### **Objectives**
Provide a consistent process to track of incidents that ensures:
 - Incidents are properly logged
 - Incidents are properly routed
 - Incident status is accurately reported
 - Queue of unresolved incidents is visible and reported
 - Incidents are properly prioritized and handled in the appropriate sequence

#### **Definitions**

##### **Impact**
Impact is determined by how many personnel or functions are affected.
There are three grades of impact:
- 3 - Low - One or two personnel. Service is degraded but still functional.
- 2 - Medium - Multiple personnel in one physical location. Service is degraded and still functional. It appears the cause of the incident falls across multiple services
- 1 - High - All personnel from one or multiple physical locations are affected

The impact of an incident will be used in determining the priority for resolution.

##### **Incident**
An incident is an unplanned interruption to an IT Service or reduction in the Quality of an IT Service. Failure of any item, software or hardware, used in the support of a system that has not yet affected service is also an Incident even though it does not interrupt service.

An incident occurs when the operational status of a production item changes from working to failing or about to fail, resulting in a condition in which the item is not functioning as it was designed or implemented. The resolution for an incident involves implementing a repair to restore the item to its original state.

A design flaw does not create an incident. If the product is working as designed, even though the design is not correct, the correction needs to take the form of a service request to modify the design. The service request may be expedited based upon the need, but it is still a modification, not a repair.

##### **Incident Repository**
The Incident Repository is a database containing relevant information about all Incidents whether they have been resolved or not. General status information along with notes related to activity should also be maintained in a format that supports standardized reporting. At Avantpage Inc, the incident repository is contained within Atlassian JIRA Service Desk.

##### **Priority**
Priority is determined by utilizing a combination of the incident's impact and severity. For a full explanation of the determination of priority refer to the paragraph titled _Priority Determination_.

##### **Response**
Time elapsed between the time the incident is reported and the time it is assigned to an individual for resolution.

##### **Resolution**
Service is restored to a point where users can perform their job. In some cases, this may only be a work around solution until the root cause of the incident is identified and corrected.

##### **Severity**
Severity is determined by how much the user is restricted from performing their work. There are three grades of severity:
- 3 - Low - Issue prevents the user from performing a portion of their duties
- 2 - Medium - Issue prevents the user from performing critical time sensitive functions
- 1 - High - Service or major portion of a service is unavailable

The severity of an incident will be used in determining the priority for resolution.

#### **Incident Scope**
The Incident process applies to all specific incidents in Avantpage Inc.

##### **Exclusions**
Request fulfillment, i.e., Service Requests are not handled by this process.
Root cause analysis of original cause of incident is not handled by this process. Refer to _Problem Management_. The need for restoration of normal service supersedes the need to find the root cause of the incident. The process is considered complete once normal service is restored.

#### **Inputs and Outputs**

| Input                        | From                             |
| ---------------------------- | -------------------------------- |
| Incident (verbal or written) | Avantpage Inc Personnel          |
| Categorization Tables        | Functional Groups                |
| Assignment Rules             | Functional Groups                |

| Output                                                | To                      |
| :---------------------------------------------------: | :---------------------: |
| Standard notifiation to the users when case is closed | Avantpage Inc Personnel |

#### **Metrics**

| Metric                                                                                                   | Purpose                                                                                                                                                  |
| -------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Process tracking metrics _n^x_ of incidents by type and status - see detail under _Reports and Meetings_ | To determine if incidents are being processed in reasonable time frame, frequency of specific types of incidents, and determine where bottlenecks exist. |

### **Chapter 2 - Roles and Responsibilities**
***

Responsibilities may be delegated, but escalation does not remove responsibility from the individual accountable for a specific action.

#### **Avantpage Inc Service Desk**
- Owns all reported incidents
- Ensure that all incidents received by the Service desk are recorded in system
- Identify nature of incidents based upon reported symptoms and categorization rules supplied by Avantpage Inc IT Department
- Prioritize incidents based upon impact on the users
- Responsible for incident closure
- Delegates responsibility by assigning incidents to the appropriate Avantpage Inc IT Department member for resolution based upon the categorization rules
- Performs post-resolution review to ensure that all work services are functioning properly and all incident documentation is complete
- Prepare reports showing statistics of Incidents resolved and/or unresolved

#### **IT Department**
- Composed of technical and functional staff involved in supporting services
- Correct the issue or provied a work around to the user that will provide functionality that approximates normal service as closely as possible
- If an incident reoccurs or is likely to reoccur, notify problem management, so that root cause analysis can be performed and a standard work around can be deployed

### **Chapter 3 - Categorization, Target Times, Prioritization and Escalation**
***

#### **Categorization**
The goals of proper categorization are:
- Identify Service impacted and appropriate escalation timelines
- Indicate which IT Department member need to be involved
- Provide meaningful metrics on system reliability

For each incident the specific service will be identified. It is critical to establish with the user the specific area of the service being provided. For example, if it's Project Management, Operations, Financial, Human Resources, or another area? If it's Financials, is it for Accounts Payable, etc.? Identifying the service properly allows to establish adequate priority level.

In addition, the severity and impact of the incident need to also be established. All incidents are important to the user, but incidents that affect large groups of personnel or mission critical functions need to be addressed before those affecting 1 or 2 people.

Does the incident cause a work stoppage for the user or do they have other means of performing their job? An example would be a broken link on a webpage is an incident, but if there is another navigation path to the desired page, the incident's severity would be low because the user can still perform the needed function.

The incident may create a work stoppage for only one person but the impact is far greater because it is a critical function. An example of this scenario would be the person processing payroll having an issue which prevents the payroll from processing. The impact affects many more personnel than just single user.

#### **Priority Determination**
The priority given to an incident that will determine how quickly it is scheduled for resolution will be set depending upon a combination of the incident severity and impact.

<- Picture ->
<- Picture ->

#### **Target Times**
Incident support for existing services is provided 24 hours per day, 7 days per week, and 365 days per year.  Following are the current targets for response and resolution for incidents based upon priority.

<- Picture ->

### **Chapter 4 - Process Flow**
***
The following is the standard incident management process flow outlined in ITIL Service Operation but represented as a swim lane chart with associated roles within Avantpage Inc.

<- Picture ->

#### **Incident Management Process Flow Steps**
<- Picture ->

### **Chapter 5 - Incident Escalation**
***
According to ITIL standards, although assignment may change, ownership of incidents always resides with the Service Desk. As a result, the responsibility of ensuring that an incident is escalated when appropriate also resides with the Service Desk.
The Service Desk will monitor all incidents, and escalate them based on the following guidelines:

#### **Functional Escalation**
Avantpage Inc does not employ an official tiered support system that utilizes escalation from internal group to external group. When the Service Desk receives notification of an incident, they are to perform the initial identification and diagnosis to classify the incident according to service category and prioritization. If the incident is a known problem with a known solution, the Service Desk will attempt a resolution. If it is not a known problem or if the attempted solution fails, they will delegate responsibility for an incident to an appropriate IT Department member.

#### **Escalation Notifications**
Any time a case is escalated, notification will occur to various individuals or groups depending upon the priority of the incident. Following are basic guidelines for notifications:
- The default mechanism for notification will be by email unless otherwise specifically stated
- Whenever escalation or notification by phone is indicated, all known numbers for contact should be utilized, leaving voice mail on each until person is contacted
- Senior management notification will include Director of Technology, and all functional managers. Escalation of a case does not remove the assignment from an individual. It is up to the Director of Technology to make certain the right personnel are assigned. When additional personnel need to be involved, they may be added as interested parties
- Any time a case is escalated, the case will be updated to reflect the escalation and the following notifications will be performed by IT Department:
  - User will receive a standard escalation email informing them of the escalation
  - Person to whom case is currently assigned will be notified
  - Manager of functional group to whom case is currently assigned will be notified

#### **Incident Escalation Process**
<- Picture ->

#### **Incident Escalation Process Steps**
All escalation process steps are performed by the Service Desk. Some of the steps may be automated.
| Step  | Description                                                                                                                                                                     |
| :---: | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.    | Examine all open incidents and determine actions based upon incident priority.                                                                                                  |
| 2.    | **Is this a priority 1 (high priority) incident?**                                                                                                                              |
| 3.    | If it is a high priority incident, immediately notify Avantpage Inc mid level and senior management personnel.  Senior management personnel should be contacted by phone.       |
| 4.    | Monitor the status of the priority 1 incident providing informational updates to management at a minimum of every 4 hours.                                                      |
| 5.    | Has the incident been resolved? If not continue to monitor.                                                                                                                     |
| 6.    | If the incident has been resolved, notify Avantpage Inc mid level and senior management of the resolution. Senior management should be notified by phone during business hours. |
| 7.    | **Is this a priority 2 (medium priority) incident?**                                                                                                                            |
| 8.    | If so, notify the manager of IT Department performing the resolution.  Notification should be by email.                                                                         |
| 9.    | Has the incident occurred during business hours or off hours?  If during business hours, proceed to step 14.                                                                    |
| 10.   | If the incident occurred during off hours, is the on call person available?                                                                                                     |
| 11.   | If the on call person is not available, call the manager of IT Department assigned for resolution.                                                                              |
| 12.   | Is the manager of IT Department available?                                                                                                                                      |
| 13.   | If neither of IT Department on-call person or the manager of IT Department is available, notify senior management via email and phone.                                          |
| 14.   | Has the time limit to resolve the incident elapsed?                                                                                                                             |
| 15.   | If the time limit to resolve has elapsed, notify the manager of IT Department via email.                                                                                        |
| 16.   | Continue to monitor the incident.                                                                                                                                               |
| 17.   | Has the incident been resolved?                                                                                                                                                 |
| 18.   | If the incident has been resolved notify all personnel previously contacted of the resolution.                                                                 |

### **Chapter 6 - Reports and Meetings**
***
A critical component of success for IT Department to hold itself accountable for deviations from acceptable performance. This will be accomplished by producing meaning reports that can be utilized to focus on areas that need improvement. The reports must then be used in coordinated activities aimed at improving the support.

#### **Reports**

##### **Service Interruptions**
A report showing all incidents related to service interruptions will be reviewed during the operational meeting. The purpose is to discover how serious the incident was, what steps are being taken to prevent reoccurrence, and if root cause needs to be pursued.

##### **Metrics**
Metrics reports should generally be produced monthly with quarterly summaries.
Metrics to be reported are:
- Total numbers of Incidents (as a control measure)
- Breakdown of incidents at each stage (e.g. logged, work in progress, closed etc)
- Size of current incident backlog
- Number and percentage of major incidents
- Elapsed time to achieve incident resolution or circumvention, broken down by impact code
- Percentage of incidents handled within response time
- Number of incidents reopened and as a percentage of the total
- Number and percentage of incidents incorrectly assigned
- Number and percentage of incidents incorrectly categorized
- Percentage of Incidents closed by the Service Desk without reference to other levels of support (often referred to as ‘first point of contact’)
- Number and percentage of incidents processed per Service Desk agent
- Number and percentage of incidents resolved remotely, without the need for a visit
- Breakdown of incidents by time of day, to help pinpoint peaks and ensure matching of resources.

##### **Meetings**
Director of Technology will conduct sessions with IT Department personnel to review performance reports.
The goal of the sessions is to identify: 
- Processes that are working well and need to be reinforced
- Patterns related to incidents where support failed to meet targets
- Reoccurring incidents where the underlying problem needs to be identified and resolution activities are pursued 
- Identification of work around solutions that need to be developed until root cause can be corrected


### **Chapter 7 - Incident Policy**
***
The Incident process should be followed for all incidents, regardless of whether the request is eventually managed as a project or through the Incident process.

If Avantpage Inc IT Department wants to significantly expand that service beyond the existing model, the request should be forwarded to the Avantpage Inc Change Management.
Incidents should be prioritized based upon impact oo the user and the availability of a workaround.

“Incident Ownership remains with the Service Desk! Regardless of where an incident is referred to during its life, ownership of the incident remains with the Service Desk at all times.  The Service Desk remains responsible for tracking progress, keeping users informed and ultimately for Incident Closure.” – _ITIL Service Operation_

Rules for re-opening incidents - Despite all adequate care, there will be occasions when incidents recur even though they have been formally closed.  If the incident recurs within one working day then it can be re-opened – but that beyond this point a new incident must be raised, but linked to the previous incident(s). 
Work arounds should be in conformance with Avantpage Inc standards and policies.
***