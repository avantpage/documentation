[![AVANTPAGE](https://i.imgur.com/so7xu2H.png)](AVANTPAGE) 


## **Linux Server Hardening Standard**
***
#### **About this document**
This document describes standards for hardening Linux Servers at Avantpage Inc.
The process provides a consistent methods for administrators to follow when hardening Linux Servers at Avantpage Inc.

#### **Who should use this document?**
This document should be used by:
 - Avantpage Inc personnel responsible for administration of servers
 - Avantpage Inc personnel involved in the operation and management of servers

### **Add non-Root user**
***
Create a limited user account and use it at all times. Administrative tasks will be done using `sudo` to temporarily elevate limited user’s privileges so you can administer your server.

> It is recommended to use strong password for every user.

### **Harden SSH Access**
***
By default, password authentication is used to connect to server via SSH. A cryptographic key-pair is more secure because a private key takes the place of a password, which is generally much more difficult to brute-force.

#### **Create an Authentication Key-pair**

This is done on local computer, not server, and will create a 4096-bit RSA key-pair. During creation, the option to encrypt the private key with a passphrase will be given. It cannot be used without entering the passphrase, unless you save it to local desktop’s keychain manager. Use the key-pair with a passphrase foir additional security layer.

### **SSH Daemon Options**
***
**Disallow root logins over SSH.** This requires all SSH connections be by non-root users.

**Disable SSH password authentication.** This requires all users connecting via SSH to use key authentication. 

**Listen on only one internet protocol.** The SSH daemon listens for incoming connections over both IPv4 and IPv6 by default.If needed to SSH into server using both protocols, disable whichever is not needed. *This does not disable the protocol system-wide, it is only for the SSH daemon.*


### **Use Fail2Ban for SSH Login Protection**
***
Fail2Ban is an application that bans IP addresses from logging into server after too many failed login attempts. Legitimate logins usually take no more than three tries to succeed (and with SSH keys, no more than one), a server being spammed with unsuccessful logins indicates attempted malicious access.

Fail2Ban can monitor a variety of protocols including SSH, HTTP, and SMTP. By default, Fail2Ban monitors SSH only, and is a helpful security deterrent for any server since the SSH daemon is usually configured to run constantly and listen for connections from any remote IP address.

Fail2ban is primarily focused on SSH attacks, although it can be further configured to work for any service that uses log files and can be subject to a compromise.

> **Caution!**
> Fail2ban is intended to be used in conjunction with an already-hardened server and should not be used as a replacement for secure firewall rules.

### **Failregexs**
***
Although Fail2ban comes with a number of filters, customize these filters or create own filters. Fail2ban uses regular expressions (regex) to parse log files, looking for instances of attempted break-ins and password failures. Fail2ban uses Python’s regex extensions.

The best way to understand how failregex works is to write one. Although it is not advised having Fail2ban monitor Wordpress’s `access.log` on heavily-trafficked websites due to CPU concerns, it provides an instance of an easy-to-understand log file that can be used to learn about the creation of any failregex.


### **Remove Unused Network-Facing Services**
***
Most Linux distributions install with running network services which listen for incoming connections from the internet, the loopback interface, or a combination of both. Network-facing services which are not needed should be removed from the system to reduce the attack surface of both running processes and installed packages.

#### **Determine Which Services to Remove**
Basic TCP and UDP `nmap` scan of server without a firewall enabled, SSH, RPC and NTPdate would be present in the result with ports open. By `configuring a firewall` filter those ports, with the exception of SSH to must allow incoming connections. However, the unused services should be disabled.
- Administer server primarily through an SSH connection. RSA keys and Fail2Ban can help protect SSH.
- An NTP daemon is one option for server’s timekeeping but there are alternatives. Method for time synchronization which does not hold open network ports, and you do not need nanosecond accuracy, then you may be interested in replacing NTPdate with OpenNTPD.
- Exim and RPC, however, are unnecessary unless there is a specific use for them, and should be removed.

Removing the offending packages will differ depending on distribution’s package manager.

### **Configure a Firewall**
***
Using a firewall to block unwanted inbound traffic to server provides a highly effective security layer. By being very specific about the traffic allowed in, prevent intrusions and network mapping. Allow only the traffic that is needed, and deny everything else.
- **Iptables** is the controller for netfilter, the Linux kernel’s packet filtering framework. Iptables is included in most Linux distributions by default.
- **FirewallD** is the iptables controller available for the CentOS / Fedora family of distributions.
- **UFW** provides an iptables frontend for Debian and Ubuntu.

### **Control Network Traffic with iptables**
***
iptables is an application that allows users to configure specific rules that will be enforced by the kernel’s `netfilter` framework. It acts as a packet filter and firewall that examines and directs traffic based on port, protocol and other criteria. This guide will focus on the configuration and application of iptables rulesets and will provide examples of ways they are commonly used.

By default, the iptables tool is included with server-supplied distribution. In order to use iptables, root (`sudo`) privileges will be needed.

### **Configure iptables**
iptables can be configured and used in a variety of ways. Configure rules by port and IP, as well as blacklist (block) and whitelist (allow) addresses.

#### **Block Traffic by Port**
Use a port to block all traffic coming in on a specific interface.

### **iptables-persistent**
Ubuntu and Debian have a package called iptables-persistent that makes it easy to reappl firewall rules at boot time. After installation, save all rules in two files (one for IPv4 and one for IPv6). After iptables rules being configured and applied, iptables-persistent will detect them automatically and allow you to add them to the appropriate configuration file.

***




















    
    